\contentsline {section}{\numberline {1}Data Link Layer Standard}{3}
\contentsline {subsection}{\numberline {1.1}Services}{3}
\contentsline {subsection}{\numberline {1.2}Framing}{3}
\contentsline {subsubsection}{\numberline {1.2.1}Header and Footer Bytes}{3}
\contentsline {subsubsection}{\numberline {1.2.2}Control Bytes}{3}
\contentsline {subsubsection}{\numberline {1.2.3}Address Bytes}{3}
\contentsline {subsubsection}{\numberline {1.2.4}Checksum Bytes}{3}
\contentsline {subsection}{\numberline {1.3}Flow Control}{3}
\contentsline {subsection}{\numberline {1.4}Error Control}{4}
\contentsline {subsubsection}{\numberline {1.4.1}Even Parity (Compulsory)}{4}
\contentsline {subsubsection}{\numberline {1.4.2}Cyclic Redundancy Check (Optional)}{4}
\contentsline {subsection}{\numberline {1.5}Media Access Control}{4}
\contentsline {section}{\numberline {2}Physical Layer Standard}{4}
\contentsline {section}{\numberline {3}Design}{5}
\contentsline {subsection}{\numberline {3.1}Toolchain}{5}
\contentsline {subsection}{\numberline {3.2}Data Link Layer}{5}
\contentsline {subsubsection}{\numberline {3.2.1}Framing}{5}
\contentsline {subsubsection}{\numberline {3.2.2}Bit Stuffing}{5}
\contentsline {subsubsection}{\numberline {3.2.3}Error Control}{5}
\contentsline {subsubsection}{\numberline {3.2.4}Transmission}{6}
\contentsline {subsubsection}{\numberline {3.2.5}Receive}{6}
\contentsline {subsection}{\numberline {3.3}Physical Layer}{6}
\contentsline {section}{\numberline {4}Testing, Results and Analysis}{7}
\contentsline {subsection}{\numberline {4.1}Error Correction (\texttt {Error.c})}{7}
\contentsline {subsection}{\numberline {4.2}Bit Stuffing (\texttt {Stuff.c})}{7}
\contentsline {subsection}{\numberline {4.3}Flow Control (\texttt {DLL.c})}{7}
\contentsline {section}{\numberline {5}Critical Reflection and Evaluation}{8}
\contentsline {subsection}{\numberline {5.1}Personal}{8}
\contentsline {subsection}{\numberline {5.2}Within Team and Peer-Team}{8}
