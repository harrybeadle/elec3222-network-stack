#include <stdlib.h>
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "STACK.h"
#include "PHY/PHY.h"

int main()
{
  packet_number = 3;
  init_stdio2uart0();
  printf("HI\n" );
  network_init();
  for(;;)
  {
    if(kbhit())
    {
      app_tx();
    }
    tickPHY();
  }

  return 0;
}
