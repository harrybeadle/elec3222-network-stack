#ifndef _DLL_H_
#define _DLL_H_

#include <stdint.h>

#include "Error.h"
#include "State.h"
#include "Frame.h"
#include "FrameTemplates.h"
#include "Stuff.h"

uint8_t rxDLL(uint8_t* data, uint8_t len);
uint8_t txDLL(uint8_t* data, uint8_t len);

uint8_t network_tx(uint8_t* buffr_in,int signal);
uint8_t network_rx(uint8_t* buffr_in,uint8_t signal);


#include "DLL.c"
#endif
