/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Framing.c
Functions used to frame the network packet passed to the DLL.

*/

#ifndef _STUFF_H_
    #define _STUFF_H_

    // Include Standard Libraries
    #include <stdint.h>

    // Include Project Libraries
    #include "Frame.h"

    // Function Prototypes
    void stuff(uint8_t *frame, uint8_t len);
    void unstuff(uint8_t *frame, uint8_t len);
    uint8_t stuffedlength(uint8_t *stuffedframe);
    
    // Unit Test Includes and Definitions
    #ifdef TEST
        #include <stdio.h>
        int main(int argc, char *argv[]);
    #endif

    // Include Source Code
    #ifndef TEST
        #include "Stuff.c"
    #endif

#endif
