/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Packets.c
Functions used to send
*/

#include "FrameTemplates.h"

void nack(uint8_t nackid)
{
    uint8_t frame[11];
    frame[HEAD] = FRAMEFLAG;
    frame[CONTH] = FNACK | FPARITY;
    frame[CONTL] = (nackid << 4) & ACKID;
    frame[LENGTH] = 0;
    frame[NET+FOOT] = FRAMEFLAG;
    frame[NET+CHECKH] = checksum(frame, METADATALENGTH);
    stuff(frame, METADATALENGTH);
    uint8_t stuffedlength = 0;
    while (frame[stuffedlength] != 0) stuffedlength++;
    // txPHY(frame, stuffedlength);
}
