/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Error.c
Functions used to detect and correct errors.

*/

#include "Error.h"

uint8_t checksum(uint8_t *frame, uint8_t len)
{
    uint8_t parity = 0;
    for (uint8_t i = 0; i < len; i++) {
        parity ^= frame[i];
    }
    return parity;
}