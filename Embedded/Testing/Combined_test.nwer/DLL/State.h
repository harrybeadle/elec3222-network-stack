/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

State.h
Variables used to store the DLL's state between function calls.

*/

#ifndef _STATE_H_
    #define _STATE_H_

    // Include Standard Libraries

    // Include Project Libraries
    #include "Frame.h"

    // Varibles and Constants
    #define BUFFERSIZE 16
    #define MAXFRAMELENGTH 36

    char sentBuffer[MAXFRAMELENGTH][BUFFERSIZE];
    uint8_t frameCounter = 0;
    uint8_t last_ack;
    uint8_t dataBuffer[128];
    uint8_t dataBufferEmpty = 1;
    uint8_t rxDataSize = 0;

    // Function Prototypes

    // Include Source Code

#endif
