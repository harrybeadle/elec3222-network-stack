/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Framing.c
Functions used to frame the network packet passed to the DLL.

*/

#include "Stuff.h"

void stuff(uint8_t *frame, uint8_t len)
{
    uint8_t buffer_length = len * 1.2; // Where ~1.17 is the theoratical maximum packet length.
    char buffer[buffer_length];

    for (uint8_t i = 0; i < len; i++) {
        buffer[i] = frame[i];
        frame[i] = 0;
    }

    char byte;
    char bit;
    uint8_t outdex = 0;
    uint8_t count = 0;
    for (uint8_t bytedex = 0; bytedex < len; bytedex++) {
        byte = buffer[bytedex];
        for (uint8_t bitdex = 0; bitdex < 8; bitdex++) {
            bit = (byte & (1 << (7 - bitdex))) >> (7 - bitdex);
            if (bit != 0) count = 0;
            else count++;
            if ((count == 5) && (bytedex != 0) && (bytedex != len - 1)) {
                // Stuff
                count = 0;
                outdex++;
                frame[outdex/8] |= 1 << (7 - (outdex % 8));
                outdex++;
            } else {
                frame[outdex/8] |= (bit != 0) << (7 - (outdex % 8));
                outdex++;
            }
        }
    }
}

void unstuff(uint8_t *frame, uint8_t len)
{
    uint8_t buffer[len];
    for (uint8_t i = 0; i < len; i++) {
        buffer[i] = frame[i];
        frame[i] = 0;
    }
    uint8_t bit = 0;
    uint8_t byte = 0;
    uint8_t outdex = 0;
    uint8_t count = 0;
    for (uint8_t bytedex = 0; bytedex < len; bytedex++) {
        byte = buffer[bytedex];
        for (uint8_t bitdex = 0; bitdex < 8; bitdex++) {
            bit = (byte & (1 << (7-bitdex))) >> (7 - bitdex);
            if (bit == 0) count++;
            if ((count == 5) && (bit == 1)) {
                count = 0;
            } else {
                frame[outdex/8] |= (bit != 0) << (7 - (outdex % 8));
                outdex++;
            }
            if (bit == 1) count = 0;
        }
    }
}

uint8_t stuffedlength(uint8_t *stuffedframe)
{
    uint8_t stuffedlen = 0;
    while (stuffedframe[stuffedlen] != 0) stuffedlen++;
    return stuffedlen;
}


#ifdef TEST
    int main(int argc, char *argv[])
    {
        // Define some data to act on.
        const uint8_t original_data[] = {
            0b10000001,
            0b10000001,
            0b10001001,
            0b10000001,
            0b11000001,
            0b10000001,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000
        };
        uint8_t data[] = {
            0b10000001,
            0b10000001,
            0b10001001,
            0b10000001,
            0b11000001,
            0b10000001,
            0b00000000,
            0b00000000,
            0b00000000,
            0b00000000
        };
        uint8_t datalength = 6;

        // Manually calculate the correct outcome.
        const uint8_t correct[] = {
            0b10000001,
            0b10000010,
            0b11000100,
            0b11000001,
            0b01110000,
            0b01110000,
            0b00100000,
            0b00000000,
            0b00000000,
            0b00000000
        };
        uint8_t stuffedlen = 7;

        // Test the Stuff Function
        printf("Testing stuff()...\n");
        stuff(data, datalength);
        // Print the Original Data
        printf("O ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (original_data[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Print the Stuffed Data
        printf("S ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (data[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Print the Original Data
        printf("C ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (correct[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Detect Errors
        for (int i = 0; i < stuffedlen; i++) {
            if (data[i] != correct[i]) {
                printf("ERROR: stuff() failed test at byte %i!\n", i);
                printf("\t0x%x should have been 0x%x.\n", data[i], correct[i]);
                return 0;
            }
        }
        printf("PASS: stuff()\n");
        
        // Test the Stuff Function
        printf("Testing unstuff()...\n");
        unstuff(data, stuffedlen);
        // Print the Original Data
        printf("O ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (original_data[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Print the Unsuffed Data
        printf("U ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (data[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Print the Stuffed Data
        printf("S ");
        for (int bytedex = 0; bytedex < stuffedlen; bytedex++){
            for (int bitdex = 0; bitdex < 8; bitdex++) {
                printf("%d", (correct[bytedex] >> (7-bitdex)) & 1);
            }
            printf(" ");
        }
        printf("\n");
        // Detect Errors
        for (int i = 0; i < stuffedlen; i++) {
            if (data[i] != original_data[i]) {
                printf("ERROR: unstuff() failed test at byte %i!\n", i);
                printf("\t0x%x should have been 0x%x.\n", data[i], original_data[i]);
                return 0;
            }
        }
        printf("PASS: unstuff()\n");

        // Test stuffedlen()
        printf("Testing stuffedlength()...\n");
        if(stuffedlength((uint8_t*) correct) != stuffedlen) {
            printf("ERROR: stuffedlenth failed.\n");
            printf("\t0x%x should have been 0x%x.\n", stuffedlength((uint8_t*) correct), stuffedlen);
            return 0;
        }
        printf("PASS: stuffedlength()\n");

        printf("\n\tALL Stuff.c TESTS PASS SUCCESSFULLY\n\n");
        return 1;
    }
#endif