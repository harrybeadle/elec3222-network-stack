/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

DLL.c
The Data Link Layer for Team B2's Stack

*/

#include "DLL.h"

uint8_t txDLL(uint8_t* data, uint8_t len)
{
    #ifdef DEBUG
    printf("### ENTER txDLL()\n");
    for (uint8_t i = 0; i < len; i++) {
      printf("%x ", data[i]);
    }
    printf("\n");
    #endif

    // Calulcate the number of frame required to transmit the packet
    uint8_t nFrames = (len / NETSEGMENTLENGTH) + 1;
    #ifdef DEBUG
    printf("nFrames %d\n", nFrames);
    #endif

    // Create arrays for the frames and their lengths
    uint8_t frames_to_send[MAXFRAMELENGTH][nFrames];
    for (uint8_t x = 0; x < MAXFRAMELENGTH; x++) {
      for (uint8_t y = 0; y < nFrames; y++) {
        frames_to_send[x][y] = 0;
      }
    }
    uint8_t frame_lengths[nFrames];

    // For each frame...
    for (uint8_t i = 0; i < nFrames; i++) {

        // Create and Pack the Frame
        uint8_t frame[MAXFRAMELENGTH];
        frame[HEAD] = FRAMEFLAG;
        frame[CONTH] = FDATA | FPARITY | (FRAMECOUNTER & nFrames - i - 1);
        frame[CONTL] = TXID & frameCounter;
        #ifdef DEBUG
        printf("***FrameCounter is %d\n", frame[CONTL] & TXID);
        #endif
        if (i == nFrames) frame[LENGTH] = len % NETSEGMENTLENGTH;
        else frame[LENGTH] = NETSEGMENTLENGTH;

        // Copy the data across into the frame
        for (uint8_t n = 0; n < frame[LENGTH]; n++) {
            frame[NET + n] = data[NETSEGMENTLENGTH * i + n];
        }

        // Fill the footer
        frame[NET + frame[LENGTH] + FOOT] = FRAMEFLAG;
        frame[NET + frame[LENGTH] + CHECKH] = checksum(frame, frame[LENGTH] + METADATALENGTH);

        #ifdef DEBUG
        printf("Pre-stuffed Frame: \n");
        for (uint8_t i = 0; i < METADATALENGTH + frame[LENGTH]; i++) {
          printf("%x ", frame[i]);
        }
        printf("\n");
        #endif

        // Stuff the frame using bit-stuffing
        stuff(frame, frame[LENGTH] + METADATALENGTH);

        // Find the stuffed length of the array
        uint8_t stuffedLength = 0;
        while (frame[stuffedLength] != 0) stuffedLength++;
        stuffedLength = METADATALENGTH + frame[LENGTH];

        // Add this frame to the buffer for NACK requests
        frame_lengths[i] = stuffedLength;
        for (uint8_t j = 0; j < frame_lengths[i]; j++) {
            sentBuffer[i][j] = frame[j];
            frames_to_send[i][j] = frame[j];
        }
        frameCounter++;

        #ifdef DEBUG
        printf("Packed Frame %d\n", i);
        #endif
    } 

    // Debugging Output
    #ifdef DEBUG
    printf("packetLength, %i \n", len);
    printf("NETSEGMENTLENGTH, %i \n", NETSEGMENTLENGTH);
    printf("nFrames, %i \n", nFrames);
    for (int i = 0; i < nFrames; i++) {
        for (int j = 0; j < frame_lengths[i]; j++) {
            printf("%x ", (unsigned int) frames_to_send[i][j]);
        }
        printf("\n");
    }
    #endif

    // Send frames the Physical layer.
    for (uint8_t n = 0; n < nFrames; n++) {
        rxDLL(frames_to_send[n], frame_lengths[n]);
        // txPHY(frames_to_send[n], frame_lengths[n]);
    }

    #ifdef DEBUG
    printf("### EXIT txDLL()\n", nFrames);
    #endif

    return 1;
}

uint8_t rxDLL(uint8_t* data, uint8_t len)
{
    #ifdef DEBUG
    uint8_t oldlen = len;
    printf("### ENTER rxDLL() \n");
    for (uint8_t i = 0; i < len; i++) {
      printf("%x ", data[i]);
    }
    printf("\n");
    #endif

    // Unstuff the Frame from the Data
    unstuff(data, len);
    len = 1;
    while (data[len] != FRAMEFLAG) len++;
    #ifdef DEBUG
    printf("Unstuffed (stuffing off) \n");
    for (uint8_t i = 0; i < oldlen; i++) {
      printf("%x ", data[i]);
    }
    printf("\n");
    printf("CONTH %x\n", data[CONTH]);
    #endif

    // Check for Data Integritity
    uint8_t chksm = data[CHECKL];
    data[CHECKL] = 0;
    if (checksum(data, len) != chksm) {
      nack(data[CONTL] & TXID);
      #ifdef DEBUG
      printf("Checksum Fail\n");
      #endif
    } else {
    #ifdef DEBUG
    printf("Checksum Pass\n");
    #endif
    }
    // Handle Acknowledgements
    if (data[CONTH] & FACK) {
      #ifdef DEBUG
      printf("Frame is ACK\n");
      #endif
      last_ack = (data[CONTL] & ACKID) >> 4;
    }
    #ifdef DEBUG
    printf("CONTH is %x\n", data[CONTH]);
    #endif
    // Handle Not Acknowledgements
    if (data[CONTH] & FNACK) {
        #ifdef DEBUG
        printf("Frame is NACK\n");
        #endif
        uint8_t nack_id = (data[CONTL] & ACKID) >> 4;
        //txPHY(sentBuffer[nack_id], sentBuffer[nack_id][LENGTH] + METADATALENGTH);
    }
    #ifdef DEBUG
    printf("CONTH is %x\n", data[CONTH]);
    #endif
    // Handle Data
    if (data[CONTH] & FDATA) {
        #ifdef DEBUG
        printf("Frame is Data\n");
        #endif
        // Initalise the State Variables if this is a new Packet
        if (dataBufferEmpty) {
            #ifdef DEBUG
            printf("Data is new.\n");
            printf("FrameCounter is %d.\n", data[CONTH] & FRAMECOUNTER);
            #endif
            dataBufferEmpty = 0;
            rxDataSize = data[CONTH] & FRAMECOUNTER;
        }
        // Fill the data into the dataBuffer
        uint8_t dataIndex = rxDataSize - data[CONTH] & FRAMECOUNTER;
        uint8_t di = 0;
        for (uint8_t i = 0; i < data[LENGTH]; i++) {
            di = dataIndex + i;
            dataBuffer[di] = data[NET + i];
        }
        #ifdef DEBUG
        printf("Data copied to buffer.\n");
        printf("Framecounter %d\n", data[CONTH] & FRAMECOUNTER);
        printf("rxDataSize %d\n", rxDataSize);
        printf("rxDS==FC? %d\n", rxDataSize == (data[CONTH] & FRAMECOUNTER));
        #endif
        // If this is the final frame in a packet transfer the data ot the NET layer
        if (rxDataSize == (data[CONTH] & FRAMECOUNTER)) {
            #ifdef DEBUG
            printf("Packet is complete.\n");
            #endif
            uint8_t len = NETSEGMENTLENGTH * (rxDataSize - 1) + data[LENGTH];
            #ifdef DEBUG
            printf("Transfered into Network Layer\n");
            for (uint8_t i = 0; i < len; i++) {
              printf("%x", dataBuffer[i]);
            }
            printf("\n");
            #endif
            network_rx(dataBuffer, len);
            for (uint8_t i = 0; i < len; i++)
                dataBuffer[i] = 0;
            dataBufferEmpty = 1;
            #ifdef DEBUG
            #endif
        }
    }


    #ifdef DEBUG
    printf("### Leave rxDLL()\n");
    #endif

    return 0;
}

#ifdef TEST

int main(void)
{
    uint8_t* msgShort = "Hello";
    uint8_t* msgLong  = "Hello! This is a test. Goodbye!";
    uint8_t* ShortExpected = {

    };
    uint8_t*

    printf("=== Starting DLL.c Testbench ===\n");
    printf("Sending short packet... ");
    txDLL(msgShort, strlen(msgShort));
    assert(strcmp(txOut, ShortExpected));
    printf("Pass.\n");
    printf("Receiving short packet... ");
    rxDLL(ShortExpected, ShortExpected[LENGTH]);
    assert(strcmp(rxOut, msgShort));
    printf("Pass.\n");
    printf("Testing for NACK with bit error... ");

    assert(1);
    printf("Pass.\n");
    printf("Sending long packet... ");

    assert(1);
    printf("Pass.\n");
    printf("Receiving long packet... ");

    assert(1);
    printf("Pass.\n");
    return 1;
}

uint8_t txPHY(uint8_t* data, uint8_t len);
uint8_t rxNET(uint8_t* data, uint8_t len);

#endif