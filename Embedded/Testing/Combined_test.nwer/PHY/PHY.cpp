/** \file main.c
 * \author Domenico Balsamo
 * \version 1.0
 * \date 2016/12/10
 */

#define __PLATFORM_AVR__

#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "rfm12.h"

#include "../DLL/DLL.h"

uint8_t txPHY(uint8_t *data, uint8_t len);
uint8_t tickPHY(void);
uint8_t initPHY(void);

uint8_t initPHY(void)
{
    _delay_ms(100);

    rfm12_init();
    _delay_ms(100);

    sei();

    return 1;
}

uint8_t txPHY(uint8_t *data, uint8_t len) {
    rfm12_tx(len, 0, data);
    return 1;
}

uint8_t tickPHY(void)
{
    if (rfm12_rx_status() == STATUS_COMPLETE) {
        rxDLL(rfm12_rx_buffer(), rfm12_rx_len());
        rfm12_rx_clear();
    }
    return 1;
}