// //center frequency to use (+- FSK frequency shift)
// #define RFM12_FREQUENCY       433170000UL
// //TX BUFFER SIZE
// #define RFM12_TX_BUFFER_SIZE  30

// //RX BUFFER SIZE (there are going to be 2 Buffers of this size for double_buffering)
// #define RFM12_RX_BUFFER_SIZE  30

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "rfm12lib/rfm12_spi.c"

#include "rfm12lib/rfm12.h"
#include "rfm12lib/rfm12_config.h"

#include "../DLL/DLL.h"

uint8_t txPHY(uint8_t *data, uint8_t len);
uint8_t tickPHY(void);
uint8_t initPHY(void);

uint8_t initPHY(void)
{
    _delay_ms(100);

    rfm12_init();
    _delay_ms(100);

    sei();
}

uint8_t txPHY(uint8_t *data, uint8_t len) {
    rfm12_tx(len, 0, data);
}

uint8_t tickPHY(void)
{
    if (rfm12_rx_status() == STATUS_COMPLETE) {
        rxDLL(rfm12_rx_buffer(), rfm12_rx_len());
        rfm12_rx_clear();
    }
}


