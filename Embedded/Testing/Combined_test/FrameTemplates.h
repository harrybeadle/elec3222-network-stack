/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

FlowControl.h
Functions used to control the flow of data through the network.

*/

#ifndef _FLOWCONTROL_H_
    #define _FLOWCONTROL_H_

    // Include Standard Libraries

    // Include Project Libraries
    #include "Frame.h"
    #include "Stuff.h"

    // Variables and Constants
    uint8_t sentbuffer[MAXFRAMELENGTH][16];

    // Function Prototypes
    uint8_t flow(uint8_t** frames, uint8_t* length, uint8_t nframes);
    uint8_t flowrx(uint8_t* data, uint8_t len);
    void nack(uint8_t nackid);

    // Include Source Code
    #include "FrameTemplates.c"

#endif
