

#include <stdlib.h>
#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <math.h>
#include "STACK.h"

#define BUFFSIZE 200
#define BDRATE_BAUD 9600
#define SOURCEPORT 0b10101010

void app_tx();
int app_rx(char*data, int orders);

int tll_tx(char* data, int orders, int length);
int tll_rx(char *data, uint8_t address);

int checksum(char * data, int length);
int kbhit();

//found here: https://secure.ecs.soton.ac.uk/notes/ellabs/1/d1/d1_code/ilmatto/embedded_coms.c
void init_stdio2uart0(void);
int uputchar0(char c, FILE *stream);
int ugetchar0(FILE *stream);

//State storing global variables
char packet_number;
char last_broadcast;

struct username{




};


int app_rx(char*data, int orders){

	if(orders == 0){
	}
	else if(orders == 6){
		printf("%s \n", data);
	}
	return 0;
}

int tll_rx(char *data, uint8_t address){

	char temp;
	temp =  (data[0] & 0b11110000);

	int length = (int) data[4];

	char* appdata = (char *) calloc(length, sizeof(char));

	for(int i = 0; i < length; i++){
		appdata[i] = data[i + 5];
	}

	printf("TLL decode: %s \n", appdata);

	if(temp == 0){
		/*recieved a broadcast message*/
	}
	else if(temp == 16){
		/*This section will return to the application layer informing it the address and port of an associated name*/
	}
	else if(temp == 32){
		/*disconnect acknowledge*/
	}
	else if(temp == 48){
		//meta
	}
	else if(temp == 64){	//message acknowledge
		char ack = 0b01111110;
			/////////////////////////////////////TODO handle message data in app_rx
	}
	else if(temp == 80){
		/*message recieved*/
	}
	else if(temp == 96){
		/*broadcast message*/
		if(last_broadcast != data[1]){
			app_rx(appdata, 6);
			last_broadcast = data[1];
		}
	}
	return 0;
}

void app_tx(){

	char input[BUFFSIZE];
	int length = 0;

	scanf("%s", input);
	printf("App Transmiting: %s \n", input);

	length = strlen(input);

	tll_tx(input, 6/*broadcast message*/, length);

	return;
}

int tll_tx(char* data, int orders, int length){

	char* tlldata;
	printf("Tll Data Recieved%s \n", data);

	int nlength = length + 8;

	tlldata = (char*) calloc(nlength, sizeof(char));

	for(int i = 0; i < length; i++){
		tlldata[i + 5] = data[i];
	}

	if(orders == 0){
		tlldata[0] |= 0b00000000;
		tlldata[3] = 0b00000000;
	}
	else if(orders == 1){
		tlldata[0] |= 0b00010000;
	}
	else if(orders == 2){
		tlldata[0] |= 0b0010000;
	}
	else if(orders == 3){
		tlldata[0] |= 0b00110000;
	}
	else if(orders == 4){
		tlldata[0] |= 0b01000000;
	}
	else if(orders == 5){
		tlldata[0] |= 0b01010000;
	}
	else if(orders == 6){
		tlldata[0] |= 0b01100000;
		tlldata[3] = 0b00110000;		///TODO temp terminates on 0 but set to 1 for now
	}

	tlldata[1] = packet_number;
	tlldata[2] = SOURCEPORT;
	tlldata[4] = length;
	data[length + 5] = 0b01111110;


	if(checksum(tlldata, nlength - 2)){
		tlldata[nlength -1] = 0b11111111;
		tlldata[nlength-2] = 0b11111111;
	}
	else{
		tlldata[nlength -1] = 0b00000000;
		tlldata[nlength- 2] = 0b00000000;
	}

	printf("Tll data sent: %s \n", tlldata);

	network_tx(tlldata, 0);

	free(tlldata);

	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////
int kbhit(){
    //return nonzero if char waiting polled version
    unsigned char b;
    b=0;
    if(UCSR0A & (1<<RXC0)) b=1;
    return b;
}

int checksum(char * data, int length){

	int i, x;
	for(i = 0; i < length; i++){
		for(int j = 0; j < 8; j++){
			if(data[i] & (1 << j)){
				x++;
			}
		}
	}
	if(x % 2 == 0){
		return 0;
	}

	return 1;
}

void init_stdio2uart0(void)			//function from WILL BARBERS LIBRARY
{
	/* Configure UART0 baud rate, one start bit, 8-bit, no parity and one stop bit */
	UBRR0H = (F_CPU/(BDRATE_BAUD*16L)-1) >> 8;
	UBRR0L = (F_CPU/(BDRATE_BAUD*16L)-1);
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);

	/* Setup new streams for input and output */
	static FILE uout = FDEV_SETUP_STREAM(uputchar0, NULL, _FDEV_SETUP_WRITE);
	static FILE uin = FDEV_SETUP_STREAM(NULL, ugetchar0, _FDEV_SETUP_READ);

	/* Redirect all standard streams to UART0 */
	stdout = &uout;
	stderr = &uout;
	stdin = &uin;
}

int uputchar0(char c, FILE *stream)
{
	if (c == '\n') uputchar0('\r', stream);
	while (!(UCSR0A & _BV(UDRE0)));
	UDR0 = c;
	return c;
}

int ugetchar0(FILE *stream)
{
	while(!(UCSR0A & _BV(RXC0)));
	return UDR0;
}
