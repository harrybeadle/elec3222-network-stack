/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Frame.h
Defines for positioning and formating of the Frame

*/

#ifndef _FRAME_H_
#define _FRAME_H_

/* Locations of Bytes in Frame */
#define HEAD   0
#define CONTH  1
#define CONTL  2
#define ADDRH  3
#define ADDRL  4
#define LENGTH 5
#define NET    6
#define CHECKH 0
#define CHECKL 1
#define FOOT   2
/* CONTH Flags and Masks */
#define FDATA        0b10000000
#define FACK         0b01000000
#define FNACK        0b00100000
#define FCRC         0b00010000
#define FPARITY      0b00000000
#define FRAMECOUNTER 0b00001111
/* CONTL Flags and Masks */
#define ACKID 0b11110000
#define TXID  0b00001111
/* Bit Stuffing */
#define FRAMEFLAG 0b10000001
/* Frame and Frame Segment Lengths */
#define NETSEGMENTLENGTH 23
#define FRAMELENGTH 32
#define MAXFRAMELENGTH 36
#define METADATALENGTH FRAMELENGTH - NETSEGMENTLENGTH

#endif