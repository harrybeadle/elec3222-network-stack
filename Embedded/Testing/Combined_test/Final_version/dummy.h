// Contains dummy functions for transport and DLL layer to test the movement of data between functions
//1 unsinged char is 1 byte
#include <stdio.h>
#include <stdbool.h>
//#include <avr/io.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define TYPE_DATA 1
#define TYPE_ROUTING 2
#define TYPE_AREQ 3
#define TYPE_AASS 4
#define ERROR_CHECKSUM 1
#define ERROR_INTER 2
#define ERROR_CRC 3
#define ERROR_F_CHECKSUM 4
#define ERROR_VERSION ERROR_F_CHECKSUM
// defining the CRC function
#define CRC_16 37195

void dummy_trans(char* input, int length);
void dummy_DLL(char* input, int length);

void dummy_DLL(char* input, int length)
{
  printf("Aessing dummy DLL function\n");
  for (int i = 0;i<length;i++)
  {
    printf("Register %d is %d \n",i,*input );
    input++;
  }

}
