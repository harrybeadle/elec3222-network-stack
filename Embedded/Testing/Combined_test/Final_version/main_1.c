#include <stdio.h>
#include <stdbool.h>
//#include <avr/io.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "NET.h"
uint8_t main()
{
  FILE *fp;
  fp = fopen ("Output.txt","w");
//
network_init();
// testing Up function with buffer
uint8_t  buffer[150];
uint8_t* buff_ptr;
buff_ptr = buffer;
// unused signal for now
int signal = 0;
int i;
// setting command bits to V1, Data type using a checksum
buffer[0] = 6;
buffer[1] = 6;
// setting SRC to 3
buffer[2] = 1;
// setting DEST to broadcast
buffer[3] = 255;
// setting  size of TRAN to 20
buffer[4] = 15;
// filling TRAN with data
for (i = 5;i< 20;i++)
{
  buffer[i] = (20);
}
//internal checksum, using a 5 offset fo pointer
buffer[20] = 40;
buffer[21] = 40;
// uint16_t trial = crc_16_calc(buff_ptr,buffer[4]);
// initalises rest of buffer with 0's
for (i = 22;i<150;i++)
{
  buffer[i] = 0;
}
// prints buffer before manipulation
fprintf(fp, "Register Start\n" );

// copies into local
test_func(buff_ptr);

for (i = 0; i < 150; i++) {
  fprintf(fp,"Register %d is %u\n",i,buffer[i]);
}

// goes down stack
  network_tx(buff_ptr,signal);

// prints buffer after tx
  fprintf(fp, "Register post tx :\n" );
  for (i = 0; i < 150; i++) {
    fprintf(fp,"Register %d is %u\n",i,buffer[i]);
  }


// runs the up function
 uint8_t result = network_rx(buff_ptr,signal);
//prints buffer after rx and returns result
  fprintf(fp, "Register post rx :\n" );
  fprintf(fp,"SRC Address is %d \n",result);
  for (i = 0; i < 150; i++) {
    fprintf(fp,"Register %d is %u\n",i,buffer[i]);
  }


  fclose(fp);
  return 0;

}
