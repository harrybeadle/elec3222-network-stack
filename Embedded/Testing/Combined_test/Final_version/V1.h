// Header File for version 1, going up
//1 unsinged char is 1 byte
#include <stdio.h>
#include <stdbool.h>
//#include <avr/io.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "dummy.h"
#define TYPE_DATA 1
#define TYPE_ROUTING 2
#define TYPE_AREQ 3
#define TYPE_AASS 4
#define ERROR_CHECKSUM 1
#define ERROR_INTER 2
#define ERROR_CRC 3
#define ERROR_F_CHECKSUM 4
#define ERROR_VERSION ERROR_F_CHECKSUM
// defining the CRC function
#define CRC_16 37195
//Defined Adress at compile time, add to compile command
// -D SYS_ADDR=1
// Function that prints the version out (will rework for il matto)
void test_func();
void network_init();
uint8_t network_rx(uint8_t* buffr_in,uint8_t signal);
uint8_t verify_version (uint8_t  input);
uint8_t packet_type_func (uint8_t  input);
uint8_t checksum_ver (uint8_t  input);
uint8_t network_tx(uint8_t* buffr_in,int signal);
uint8_t checksum_calc(uint8_t* buffr_in,uint8_t length);
uint8_t parity_calc(uint8_t* buffr_in,uint8_t length);
uint8_t split_16_1(uint16_t input);
uint8_t split_16_2(uint16_t input);
uint16_t crc_16_calc(uint8_t* buffr_in,uint8_t length);
uint16_t fletchers_checksum(uint8_t* buffr_in,uint8_t length);
void copy_buffer_in(uint8_t* buffr_in,uint8_t* local_buffer,uint8_t size);
void print_local_buffer(uint8_t* buffr_in);
void print_local_buffer_ext();

uint8_t internal_buffer[150];
uint8_t* buff_ptr;

void network_init()
{
  buff_ptr = internal_buffer;
}

void test_func(uint8_t* buffr_in)
{
  printf("Version 1 has been selected \n");
  copy_buffer_in(buffr_in,buff_ptr,150);
  //print_local_buffer(buff_ptr);
}

// Main function for going from the DLL to Transport
uint8_t network_rx(uint8_t* buffr_in,uint8_t signal)
{
  printf("Up function called\n" );
  uint8_t temp;
  uint8_t i;
  // extracts the control bits
  uint8_t   control = *buffr_in;
  buffr_in = buffr_in + 2;
  //printf("Control Bit Read as %u\n", control);

  //extracts Src Address
  uint8_t  SRC_addr = *buffr_in;
  buffr_in = buffr_in + 1;
  //printf("SRC Read as %u\n", SRC_addr);

  //Extracts Destination address
  uint8_t  DEST_addr = *buffr_in;
  buffr_in = buffr_in + 1;
  //printf("DEST Read as %u\n", DEST_addr);

  // Extracts the length
  uint8_t  length = *buffr_in;
  // resets pointer
  buffr_in = buffr_in - 4;

  // gets value of first 2 bits and runs a version check, if it fails extis the function
  uint8_t verification = verify_version(control);
  if (verification != 1)
  {
    printf("Incorrect verision, version %u is dtected\n", verification );
    return 0;
  }
  else
  {
    printf("Version 1 has been detected\n");
  }

  // Determines Type of data
  //uint8_t packet_type = packet_type_func(control);
  // Determines the checksum Type
  uint8_t checksum_type = checksum_ver(control);
  //printf("Checksum type: %u\n", checksum_type );

  if (checksum_type == ERROR_CHECKSUM)
  {
    uint8_t total_remainder = checksum_calc(buffr_in,length);
    // for some reason the pointer resets to the 1st cell here and must be shifted back
    buffr_in = buffr_in + length + 6;
    // gets that value of the checksum (only the 2nd byte is used in checksum)
    temp = *buffr_in;
    uint8_t checksum = temp;
    // Resets the pointer
    buffr_in = buffr_in - (length + 6);
    // if the two match, porgram continues, otherwise exits with error -2
    //printf("Checksum is %u , total is %u\n",checksum,total_remainder);
    if (checksum != total_remainder)
    {
      printf("Network_up has detected the checksum is incorrect\n" );
      return -2;
    }

  }

  if (checksum_type == ERROR_INTER)
  {
    //run interleaved even parity
    uint8_t checksum_parity = parity_calc(buffr_in,length);

    buffr_in = buffr_in + length + 6;
    temp = *buffr_in;
    uint8_t checksum = temp;

    buffr_in = buffr_in - (length + 6);
    //printf("Parity byte is %u , total is %u, length is %u\n",checksum,checksum_parity,length);
    if (checksum != checksum_parity)
    {
      printf("Network_up has detected the parity checksum is incorrect\n" );
      return -2;
    }
  }

  if (checksum_type == ERROR_CRC)
  {
    // do CRC
    uint16_t CRC_checksum  = crc_16_calc(buffr_in,length);
    uint16_t CRC_input = 0;
    buffr_in =  buffr_in + 8 + 5;
    CRC_input = *buffr_in;
    CRC_input <<= 8;
    buffr_in++;
    CRC_input = *buffr_in;
    if (CRC_input != CRC_checksum)
    {
      printf("Network_up has detected the CRC is incorrect\n" );
      return -2;
    }
    // resets pointer
    buffr_in =  buffr_in - (8 + 6);
  }
  if (checksum_type == ERROR_F_CHECKSUM)
  {
    //run fletchers checksum
    uint16_t f_checksum =  fletchers_checksum(buffr_in,length);
    buffr_in = buffr_in + length + 5;
    uint16_t f_checksum_packet = 256*(*buffr_in);
    buffr_in++;
    f_checksum_packet = f_checksum_packet + *buffr_in;
    buffr_in = buffr_in - (length + 6);
    if (f_checksum != f_checksum_packet)
    {
      printf("Network_up has detected the flethchers checksum is incorrect\n" );
      return -2;
    }
  }
// checks to see if packet is for this machine or on broadcast
  if (DEST_addr != SYS_ADDR)
  {
    if(DEST_addr != 255)
    {
      printf(" DEST is wrong: %u\n",DEST_addr );
      return 0;
    }
  }
  // Removes header from buffer and moves TRAN to start
  for (i = 0;i<length;i++)
    {
    buffr_in = buffr_in+5;
    temp = *buffr_in;
    buffr_in = buffr_in-5;
    *buffr_in = temp;
    buffr_in++;
    }
  //deletes unused chars from memory
  uint8_t  clear = 0;
  for (i = 0;i<7;i++)
  {
    //printf("Setting %u to clear\n", *buffr_in );
    *buffr_in = clear;
    buffr_in++;
  }
  //Resets the pointer
  buffr_in = buffr_in - (length + 7);
  // Stores completed buffer into memory
  copy_buffer_in(buffr_in,buff_ptr,150);
  // Passes adress of sender up the stack with buffer
  // tll_rx(buff_ptr,SRC_addr);
  return 0;
}

uint8_t verify_version (uint8_t  input)
{
  uint8_t result = 1;
  uint8_t temp = input;
  if (temp > 127)
  {
    result = result + 2;
    temp = temp -128;
  }
  if (temp > 127)
  {
    result = result + 1;
  }

  return result;
}
// function called to get the packet type, only called after verification, so assumes char < 63
uint8_t packet_type_func (uint8_t  input)
{
  uint8_t temp = input;
  uint8_t result = 1; // if no bits are found, assumes type 1
  if (temp > 31)
  {
    temp = temp - 32;
    result = result + 2;
  }
  if(temp > 15)
  {result++;}

  return result;
}

uint8_t checksum_ver (uint8_t  input)
{
  uint8_t temp = input;
  uint8_t result = 1;
  if (temp & 0x04)
  {result = result + 2;}
  if (temp & 0x02)
  {result = result + 1;}
  //printf("Checksum Version %u delected\n", result);
  return result;
}

uint8_t network_tx(uint8_t* buffr_in,int signal)
{
  // Starts Function
  printf("TX func called\n" );
  int i;
  uint8_t temp,length;
  // set the pointer to the length and get the length and Resets pointer
  buffr_in = buffr_in + 4;
  temp  = *buffr_in;
  buffr_in = buffr_in - 4;
  length = temp + 7;
  //printf("Length is %u",length);
  // shifts all values 5 bytes along to make room for new Header
  buffr_in = buffr_in + length;
  for (i = 0;i<=length;i++)
  {
    temp  = *buffr_in;
    buffr_in = buffr_in + 5;
    *buffr_in = temp;
    buffr_in = buffr_in - 6;
  }
  // setting new length
  buffr_in = buffr_in + 5;
  //printf(" length is %u\n", length );
  *buffr_in = length;
  buffr_in--;
  //setting DEST_addr
  *buffr_in = 255;
  buffr_in--;
  // Setting SRC address
  *buffr_in = SYS_ADDR;
  buffr_in--;
  //Setting Control Bit 2 (unused)
  *buffr_in = 0;
  buffr_in--;
  // Setting Control Bit 1(only checksum type in V1)
  if(ERROR_VERSION == ERROR_CHECKSUM)
  {*buffr_in = 0;}
  if(ERROR_VERSION == ERROR_INTER)
  {*buffr_in = 2;}
  if(ERROR_VERSION == ERROR_CRC)
  {*buffr_in = 4;}
  if(ERROR_VERSION == ERROR_F_CHECKSUM)
  {*buffr_in = 6;}
  // Calculating checksum defined above

  if(ERROR_VERSION == ERROR_CHECKSUM)
  {
    //WRONG FIX//////////////////////////////////////////////////
    // counts all 1s in packet
    uint8_t checksum = checksum_calc(buffr_in,length);
    // sets checksum bytes
    buffr_in = buffr_in + length + 6;
    *buffr_in = checksum;
    // resets pointer
    buffr_in = buffr_in - (length + 6);
  }
  if(ERROR_VERSION == ERROR_INTER)
  {
    // do interleaved checksum
    uint8_t parity = parity_calc(buffr_in,length);
    buffr_in =  buffr_in + length + 6;
    *buffr_in = parity;
    // resets pointer
    buffr_in =  buffr_in - (length + 6);
  }
  if(ERROR_VERSION == ERROR_CRC)
  {
    // do CRC
    uint16_t CRC_checksum  = crc_16_calc(buffr_in,length);
    buffr_in =  buffr_in + length + 5;
    *buffr_in = split_16_1(CRC_checksum);
    buffr_in++;
    *buffr_in = split_16_2(CRC_checksum);
    // resets pointer
    buffr_in =  buffr_in - (length + 6);

  }
  if(ERROR_VERSION == ERROR_F_CHECKSUM)
  {
    // Do fletchers_checksum
    uint16_t f_checksum =  fletchers_checksum(buffr_in,length);
    buffr_in =  buffr_in + length + 5;
    *buffr_in = split_16_1(f_checksum);
    buffr_in++;
    *buffr_in = split_16_2(f_checksum);
    // resets pointer
    buffr_in =  buffr_in - (length + 6);
  }

  //copies into local_buffer
  copy_buffer_in(buffr_in,buff_ptr,150);

  // Calls Dll function
  dummy_DLL(buff_ptr,(length+7));
  return 0;
}


uint8_t checksum_calc(uint8_t* buffr_in,uint8_t length)
{
  //printf("checksum_calc called using length %u\n",length);
  uint16_t sum;
  for (uint8_t i=0;i<(length + 5); i++)
  {
    sum  = sum + *buffr_in;
    buffr_in++;
    //printf("SUM: %u\n",sum );
  }
  uint8_t result  = sum % 256;
  return result;
}

uint8_t parity_calc(uint8_t* buffr_in,uint8_t length)
{
  uint8_t columns[8] = {0,0,0,0,0,0,0,0};
  uint8_t result = 0;
  uint8_t i = 0;
  uint8_t temp = 0;
  for(i = 0; i<(length + 5); i++)
  {
    //printf("pointer: %u Length: %u\n", *buffr_in,length );
    // gather number of bits in each column
    for (uint8_t j = 0;j<8;j++)
      {
        temp = *buffr_in;

        uint8_t power = 1;
        for (uint8_t k = 0; k<j;k++)
        {
           power = power<<1;
        }
        temp  = temp & power;
        //printf ("temp post ADD is %u, from input %u with mask %u\n",temp,*buffr_in,power);
        if(temp != 0 )
        {
          columns[j]++;
        }
      }
    buffr_in++;
    //printf("Columns vector is %u %u %u %u %u %u %u %u, temp was %u \n",columns[0],columns[1],columns[2],columns[3],columns[4],columns[5],columns[6],columns[7],*buffr_in);
  }
  // if odd, add 1 to parity byte
  for (i = 0;i<8;i++)
  {
      if(columns[i]%2 == 1)
      {
        result = result + (1<<i);
        //printf("checksum is %u \n",result );
      }
  }
  return result;
}


//
uint16_t fletchers_checksum(uint8_t* buffr_in,uint8_t length)
{
  uint16_t result = 0;
  // creates the two variables for storage
  uint16_t temp1 = 0;
  uint16_t temp2 = 0;
  uint8_t i,temp_in;
  for (i=0;i< (length+5);i++)
  {
    temp_in = *buffr_in;
    temp1 = (temp1 + temp_in);
    temp2 = (temp2 + temp1);
    //printf("|Input: %u |Temp1: %u |Temp2: %u|\n",temp_in,temp1,temp2 );
    buffr_in++;
  }
  temp2 = 255 - (temp2%255);
  temp1 = 255 - (temp1%255);
  result = (temp2*256) + temp1;
  //printf("Final Result: %u, made of temp2: %u and temp1: %u \n",result,temp2,temp1 );
  return result;
}

uint8_t split_16_2(uint16_t input)
{
  uint8_t result = input%256;
  return result;
}

uint8_t split_16_1(uint16_t input)
{
  uint8_t result = input>>=8;
  return result ;
}

// this sum is based of psuedo code given by tg8g16
uint16_t crc_16_calc(uint8_t* buffr_in, uint8_t length)
{
  uint16_t temp;
  uint8_t temp_short;
  // loads in the inital remainder
  temp_short = *buffr_in;
  temp = temp_short;
  temp <<= 8;
  buffr_in++;
  temp = temp + *buffr_in;
  buffr_in++;
  // cycles through bytes
  for (uint8_t i =0; i< (length+3);i++)
  {
    temp_short = *buffr_in;
    // cycles through bits within the bytes
    for (uint8_t j = 0;j<8;j++)
    {
      // if the first bit of temp is 1
      if((temp & 0x8000) == 1)
      {
        temp ^= CRC_16;
      }
      // shifts the bits to allow for next one
      temp = (temp & 0x7FFF) << 1;
      // adds in new bit
      temp = temp | ((temp_short >> (7- j)) & 0x0001);
    }
    // moves pointer to next byte
    buffr_in++;
  }
  // Calculating crc
  uint8_t result1;
  uint8_t result2;
  result1 ^= (temp >> 8) & 0x00FF;
  result2 ^= temp & 0x00FF;
  temp = (result1*256) + result2;
  return temp;
}

void copy_buffer_in(uint8_t* buffr_in,uint8_t* local_buffer,uint8_t size)
{
  for (uint8_t i = 0; i< size; i++)
  {
    *local_buffer = *buffr_in;
    //printf("Register %d is now %u\n",i, *local_buffer );
    buffr_in++;
    local_buffer++;
  }
  return;
}
void print_local_buffer(uint8_t* buffr_in)
{
  for (uint8_t i = 0; i < 150; i++) {
    printf("Byte %d is %u\n",i,*buffr_in );
    buffr_in++;
  }
}

void print_local_buffer_ext()
{
  for (uint8_t i = 0; i < 150; i++) {
    printf("Byte %d is %u\n",i,*buff_ptr );
    buff_ptr++;
  }
}
