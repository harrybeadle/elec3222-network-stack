// Main header file for the NETWORK layer
// ta3g16
#ifndef VERSION_SELECT
  #define VERSION_SELECT 1

  #if(VERSION_SELECT == 0)
  #include "test.h"
  #endif
  #if(VERSION_SELECT == 1)
  #include "V1.h"
  #endif
  #if(VERSION_SELECT == 2)
  #include "V2.h"
  #endif
  #if(VERSION_SELECT == 4)
  #include "V4.h"
  #endif
#endif
