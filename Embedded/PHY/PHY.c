#include "rfm12lib/rfm12_config.h"
#include "rfm12lib/rfm12.h"
#include "rfm12lib/uart.h"

#include "rfm12lib/uart.cpp"
#include "rfm12lib/rfm12.cpp"

#include "../DLL/DLL.h"

uint8_t txPHY(uint8_t *data, uint8_t len);
uint8_t tickPHY(uint8_t *data, uint8_t len);
uint8_t initPHY(void);

uint8_t initPHY(void)
{
    init_uart0();
    _delay_ms(100);

    rfm12_init();
    _delay_ms(100);

    sei();
}

uint8_t txPHY(uint8_t *data, uint8_t len) {
    rfm12_tx(len, 0, data);
}

uint8_t tickPHY(uint8_t *data, uint8_t len)
{
    if (rfm12_rx_status() == STATUS_COMPLETE) {
        rxDLL(rfm12_rx_buffer(), rfm12_rx_len());
        rfm12_rx_clear();
    }
}


