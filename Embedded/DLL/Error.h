/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

rror.h
Functions used to detect and correct errors.

*/

#ifndef _ERROR_H_
    #define _ERROR_H_

    // Include Standard Libraries
    #include <stdint.h>

    // Function Prototypes
    uint8_t checksum(uint8_t *frame, uint8_t len);

    // Include Source Code
    #ifndef TEST
        #include "Error.c"  
    #endif

    // Add Prototypes and Libraries for Hosted Debugging
    #ifdef TEST
        #include <assert.h>
        #include <stdio.h>
        int main(void);
    #endif

#endif
