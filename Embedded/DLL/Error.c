/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Error.c
Functions used to detect and correct errors.

*/

#include "Error.h"

uint8_t checksum(uint8_t *frame, uint8_t len)
{
    uint8_t parity = 0;
    for (uint8_t i = 0; i < len; i++) {
        parity ^= frame[i];
    }
    return parity;
}

#ifdef TEST
int main(void)
{

    uint8_t data[] = {
        0b10011100,
        0b11001010,
        0b11101000,
        0b11101110,
        0b11011110,
        0b11100100,
        0b11010110
    };

    printf("=== Starting Error.c Testbench ===\n");
    printf("Calculating checksum: calling checksum()... ");
    uint8_t chk = checksum(data, sizeof(data)/sizeof(data[0]));
    printf("Pass.\n");
    printf("Checking against expected result... ");
    assert(chk == 0b10111100);
    printf("Pass.\n");
    return 1;

}
#endif