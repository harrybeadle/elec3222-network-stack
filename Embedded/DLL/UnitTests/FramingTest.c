#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include <stdint.h>

#define DEBUG

uint8_t rxNET(uint8_t* data, uint8_t len)
{
    printf("%s", (char*) data);
    return 1;
}

uint8_t txPHY(uint8_t* data, uint8_t len)
{
    printf("%s", (char*) data);
    return 1;
}

#include "../FlowControl.h" 
#include "../Framing.h"
// #include "../dll.h"

int main(void)
{
    printf("### Start Test ###\n");

    // // Test Bit Stuffing
    // char data[6] = {
    //     0b10000001,
    //     0b00000000,
    //     0b01010001,
    //     0b11010001,
    //     0b10000001,
    //     0b00000000
    // };
    // stuff(data, 5);
    // printf("Bitstuff %x", data);
    // // assert(assertion)

    uint8_t *p = (uint8_t *) "Hello World!";
    pack(p, strlen((char*) p));
    p = (uint8_t *) "This is my packet! It's a lovely colour.";
    pack(p, strlen((char*) p));

    // for (int i = 0; i < 2; i++) { 
    //     rxDLL((char*) sentBuffer[i], MAXFRAMELENGTH * 1.2);
    // }

    printf("### End Test ###");
    return 1;
}