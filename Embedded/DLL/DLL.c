#include "DLL.h"

#include "Frame.h"

uint8_t txDLL(uint8_t* data, uint8_t len)
{
    #ifdef DEBUG
    printf("### ENTER txDLL()\n");
    #endif

    // Calulcate the number of frame required to transmit the packet
    uint8_t nFrames = (packetLength / NETSEGMENTLENGTH) + 1;
    #ifdef DEBUG
    printf("nFrames %d\n", nFrames);
    #endif

    // Create arrays for the frames and their lengths
    uint8_t frames_to_send[MAXFRAMELENGTH][nFrames];
    uint8_t frame_lengths[nFrames];

    // For each frame...
    for (uint8_t i = 0; i < nFrames; i++) {
        
        // Create and Pack the Frame
        uint8_t frame[MAXFRAMELENGTH];
        frame[HEAD] = FRAMEFLAG;
        frame[CONTH] = FDATA | FPARITY | (FRAMECOUNTER & nFrames - i - 1);
        frame[CONTL] = TXID & frameCounter;
        if (i == nFrames) frame[LENGTH] = packetLength % NETSEGMENTLENGTH;
        else frame[LENGTH] = NETSEGMENTLENGTH;
        
        // Copy the data across into the frame
        for (uint8_t n = 0; n < frame[LENGTH]; n++) {
            frame[NET + n] = packet[NETSEGMENTLENGTH * i + n];
        }
        
        // Fill the footer
        frame[NET + frame[LENGTH] + FOOT] = FRAMEFLAG;
        frame[NET + frame[LENGTH] + CHECKH] = checksum(frame, frame[LENGTH] + METADATALENGTH);
        
        // Stuff the frame using bit-stuffing
        #ifdef STUFF
        stuff(frame, frame[LENGTH] + METADATALENGTH);
        
        // Find the stuffed length of the array
        uint8_t stuffedLength = 0;
        while (frame[stuffedLength] != 0) stuffedLength++;
        #endif

        // Add this frame to the buffer for NACK requests
        frame_lengths[i] = stuffedLength;
        for (uint8_t j = 0; j < frame_lengths[i]; j++) {
            sentBuffer[i][j] = frame[j];
            frames_to_send[i][j] = frame[j];
        }
        frameCounter++;

        #ifdef DEBUG
        printf("Packed Frame %d\n", i);
        #endif
    }

    // Debugging Output on Host
    #ifdef DEBUG
    printf("packetLength, %i \n", packetLength);
    printf("NETSEGMENTLENGTH, %i \n", NETSEGMENTLENGTH);
    printf("nFrames, %i \n", nFrames);
    for (int i = 0; i < nFrames; i++) {
        for (int j = 0; j < frame_lengths[i]; j++) {
            printf("%x ", (unsigned int) frames_to_send[i][j]);
        }
        printf("\n");
    }
    #endif

    // Send frames the Physical layer.
    for (uint8_t n = 0; n < nFrames; n++) {
        txPHY(frames_to_send[n], frame_lengths[n]);
    }

    #ifdef DEBUG
    printf("### EXIT txDLL()\n", nFrames);
    #endif

    return 1;
}

uint8_t rxDLL(uint8_t* data, uint8_t len)
{
    // Unstuff the Frame from the Data
    unstuff(data, len);
    len = 1;
    while (data[len] != FRAMEFLAG) len++;
    // Check for Data Integritity
    uint8_t chksm = data[CHECKL];
    data[CHECKL] = 0;
    if (checksum(data, len) != chksm)
        nack(data[CONTL] & TXID);
    // Handle Acknowledgements
    if (data[CONTH] & FACK) 
        last_ack = (data[CONTL] & ACKID) >> 4;
    // Handle Not Acknowledgements
    if (data[CONTH] & FNACK) {
        uint8_t nack_id = (data[CONTL] & ACKID) >> 4;
        txPHY(sentBuffer[nack_id], sentBuffer[nack_id][LENGTH] + METADATALENGTH);
    }
    // Handle Data
    if (data[CONTH] & FDATA) {
        // Initalise the State Variables if this is a new Packet
        if (dataBufferEmpty) {
            dataBufferEmpty = 0;
            rxDataSize = data[CONTH] & FRAMECOUNTER;
        }
        // Fill the data into the dataBuffer
        uint8_t dataIndex = rxDataSize - data[CONTH] & FRAMECOUNTER;
        uint8_t di = 0;
        for (uint8_t i = 0; i < data[LENGTH]; i++) {
            di = dataIndex + i;
            dataBuffer[di] = data[NET + i];
        }
        // If this is the final frame in a packet transfer the data ot the NET layer
        if (rxDataSize == dataIndex) {
            uint8_t len = NETSEGMENTLENGTH * (rxDataSize - 1) + data[LENGTH];
            rxNET(dataBuffer, len);
            for (uint8_t i = 0; i < len; i++)
                dataBuffer[i] = 0;
            dataBufferEmpty = 1;
        }
    }
    
}

