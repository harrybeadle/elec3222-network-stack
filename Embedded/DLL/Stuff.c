/*

Harry Beadle
Group B2

ELEC3222 Computer Networks
Network Stack Coursework

DLL

Framing.c
Functions used to frame the network packet passed to the DLL.

*/


#include "Stuff.h"

void stuff(uint8_t *frame, uint8_t len) 
{
    char buffer[len];

    for (uint8_t i = 0; i < len; i++) {
        buffer[i] = frame[i];
        frame[i] = 0;
    }

    char byte;
    char bit;
    uint8_t outdex = 0;
    uint8_t count = 0;
    for (uint8_t bytedex = 0; bytedex < len; bytedex++) {
        byte = buffer[bytedex];
        for (uint8_t bitdex = 0; bitdex < 8; bitdex++) {
            bit = (byte & (1 << (7 - bitdex))) >> (7 - bitdex);
            if (bit != 0) count = 0;
            else count++;
            if ((count == 6) && (bytedex != 0) && (bytedex != len - 1)) {
                // Stuff
                count = 0;
                frame[outdex/8] |= 1 << (7 - (outdex % 8));
                outdex++;
            }
            frame[outdex/8] |= (bit != 0) << (7 - (outdex % 8));
            outdex++;
        }
    }
}

void unstuff(uint8_t *frame, uint8_t len)
{
    uint8_t buffer[len];
    for (uint8_t i = 0; i < len; i++) {
        buffer[i] = frame[i];
        frame[i] = 0;
    }
    uint8_t bit = 0;
    uint8_t byte = 0;
    uint8_t outdex = 0;
    uint8_t count = 0;
    for (uint8_t bytedex = 0; bytedex < len; bytedex++) {
        byte = buffer[bytedex];
        for (uint8_t bitdex = 0; bitdex < 8; bitdex++) {
            bit = (byte & (1 << (7-bitdex))) >> (7 - bitdex);
            if (bit == 0) count++;
            if (count==5 && bit==1) continue;
            else {
                frame[outdex/8] |= (bit != 0) << (7 - (outdex % 8));
                outdex++;
            }
        }
    }
}