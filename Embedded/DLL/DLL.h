#ifndef _DLL_H_
#define _DLL_H_

#include <stdint.h>

#include "Error.h"

#define STUFF

#ifndef TEST
#include "dll.c"
#endif

#ifdef TEST
#include <stdio.h>
#include <string.h>

uint8_t txOut[100];
uint8_t rxOut[100];

int main(void);
uint8_t txPHY(uint8_t* data, uint8_t len);
uint8_t rxNET(uint8_t* data, uint8_t len);


#endif