
f = [0b10000001, 0b00000000, 0b01010001, 0b11010001, 0b10000001]

# n = 0
# count = 0
# while (1):
#     ba = f[int(n/8)] | (0x00 << 8)
#     bb = f[int(n/8)+1]
#     sec = bb | (ba<<8)
#     mask = (1 << (15 - (n%16)))
#     print("n = {0}".format(n))
#     print("{0:b}".format(sec))
#     print("{0:0>16b} {1}".format(mask, n%16))
#     if (mask & sec): count = 0
#     else: count += 1
#     print(" " * (n%16) + "^ " + str(count))
#     n += 1
#     if (n%8 == 0) and (f[int(n/8)+1] == 0b10000001):
#         break
#     print('')

DEBUG = True
delimiter = 0b10000001
o = [0 for x in range(12)] # Do some maths to be memory efficent TODO
count = 0
o[0] = delimiter
outdex = 9
nstuff = 0
for bytedex in range(1, len(f)-1):
    byte = f[bytedex]
    print(bytedex, byte)
    if (bytedex == 0) or (bytedex == (len(f) - 1)):
        if DEBUG: print("Skip Start/End Byte\n")
        #break
    for bitdex in range(8):
        bit = (byte & (1 << (7 - bitdex))) >> (7 - bitdex)
        if (bit): count = 0
        else: count += 1
        if (count == 5):
            if DEBUG: print("Stuff!")
            nstuff += 1
            count = 0
            o[int(outdex/8)] |= 1 << (7 - (outdex % 8))
            #outdex += 1
        o[int(outdex/8)] |= bit << (7 - (outdex % 8))
        outdex += 1
        if DEBUG:
            print("B{byte: <2}, b{bit: <2}".format(byte=bytedex, bit=bitdex))
            print("{0:0>8b}".format(byte))
            print("{0:0>8b}".format(bit))
            print(" " * (bitdex) + '^' + "({0})".format(bit))
            print("Progress:")
            print(" " * nstuff, end='')
            for b in f:
                print("{0:0>8b}".format(b), end='')
            print('')
            for b in o:
                print("{0:0>8b}".format(b), end='')
            print('')
            print(" " * (outdex-1) + "^")
            print('\n')
for i in range(8):
    o[int((outdex + i)/8)] |= (delimiter & (1 << (7 - i))) >> (7 - i) << (7 - ((outdex + i) % 8))


print("Final:")
for b in f:
    print("{0:0>8b}".format(b), end='')
print('')
for b in o:
    print("{0:0>8b}".format(b), end='')
print('')
print(" " * (outdex-1) + "^")
print('\n')