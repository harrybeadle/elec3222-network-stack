/*

ELEC3222 Computer Networks Coursework

Team B2:
Thomas Abery (NET)
Harry Beadle (DLL, PHY)
Christian Clarke (APP, TLL)

Shared headerfile defining protocol layer services and shared libraries.

A: Harry Beadle 2018-10-31

txYYY() is called by the layer above YYY while transmitting data down the stack.
rxYYY() is called by the layer below YYY while recieveing data up the stack.

*/

#ifndef _STACK_H_

    // Shared Libraries
    #include <stdint.h>

    // Application Layer
    uint8_t txAPP(uint8_t *data, uint8_t len);
    uint8_t rxAPP(uint8_t *data, uint8_t len);

    // Transport Layer
    uint8_t txTLL(uint8_t *data, uint8_t len);
    uint8_t rxTLL(uint8_t *data, uint8_t len);

    // Network Layer
    uint8_t txNET(uint8_t *data, uint8_t len);
    uint8_t rxNET(uint8_t *data, uint8_t len);

    // Data Link Layer
    uint8_t txDLL(uint8_t *data, uint8_t len);
    uint8_t rxDLL(uint8_t *data, uint8_t len);

    // Physical Layer
    uint8_t txPHY(uint8_t *data, uint8_t len);
    uint8_t rxPHY(uint8_t *data, uint8_t len);

    #define _STACK_H_

#endif